# -*- coding: utf-8 -*-
"""
Created on Mon May 28 20:25:56 2018

@author: KPR
"""


from datetime import datetime
from xlstodictionary import read
from gmplot import gmplot
from math import ceil


sections_and_jns = read('division_map.xls')
sections_and_srs = read('speed-restrictions.xls')
gmap = gmplot.GoogleMapPlotter(25.837072824922515, 93.43920269119431, 10)

all_sections = set()
all_junctions = set()


class section(object):
    def __init__(self, section_name):
        self.section_name = section_name
        self.list_of_junctions = list()
        self.list_of_srs = list()

    def add_jn(self, junction, kms):
        self.list_of_junctions.append(jn_in_sn(junction, kms))
        return self

    def add_sr(self, sr):
        self.list_of_srs.append(sr)
        return self

    def locate_jn_in_section(self, junction):

        relevant_list = self.list_of_junctions

        output = list()

        for junction_object in relevant_list:

            if len(output) != 0:
                return output[0]
            else:
                if junction_object.junction == junction:
                    return junction_object
                else:
                    pass
        return output

    def __repr__(self):
        string = str(self.section_name) + ' has the junctions ' + str(self.list_of_junctions) + ' and the speed-restrictions at ' + str(self.list_of_srs)
        return string


class jn_in_sn(object):
    def __init__(self, junction, milestone):
        self.junction = junction
        self.milestone = milestone

    def __repr__(self):
        output = str(self.junction) + ' is at ' + str(self.milestone) + ' in this section '
        return output


class junction(object):
    def __init__(self, jn_name, longitude, latitude):
        self.jn_name = jn_name
        self.longitude = longitude
        self.latitude = latitude

    def __repr__(self):
        return str(self.jn_name)


class sub_section(object):
    def __init__(self, section, start_milestone, end_milestone, speed_limit, jns_in_section, kms_in_section, line_nos, date=datetime.now(), up_or_down='na'):
        self.section = section
        self.start_milestone = start_milestone
        self.end_milestone = end_milestone
        self.speed_limit = speed_limit
        self.date = date
        self.line_nos = line_nos
        self.up_or_down = up_or_down
        self.preceding_jn, self.succeeding_jn = self.find_preceding_succeeding_junction()
        self.intermediate_jns = self.get_intermediate_jns(self.preceding_jn, self.succeeding_jn, jns_in_section, kms_in_section)
        actual_sr_start, actual_sr_end = self.locate_start_and_end_of_SR()
        self.start_t = junction(actual_sr_start, actual_sr_start[0], actual_sr_start[1])
        self.end_t = junction(actual_sr_end, actual_sr_end[0], actual_sr_end[1])
        self.list_of_junctions = [self.start_t] + self.intermediate_jns + [self.end_t]
        if self.line_nos == 'DL':
            doubling_factor = 100
        else:
            doubling_factor = 0
        if len(self.list_of_junctions) > 1:
            upper_list_jns = [(self.start_t.longitude, self.start_t.latitude)]
            lower_list_jns = [(self.start_t.longitude, self.start_t.latitude)]
            for i in range(len(self.list_of_junctions) - 1):
                upper_list_jns_app = self.find_perpendicular(self.list_of_junctions[i], self.list_of_junctions[i+1], doubling_factor)[0]
                lower_list_jns_app = self.find_perpendicular(self.list_of_junctions[i], self.list_of_junctions[i+1], doubling_factor)[1]
                upper_list_jns += upper_list_jns_app
                lower_list_jns += lower_list_jns_app
            end_of_section = [(self.end_t.longitude, self.end_t.latitude)]
            final_list = self.list_reducer(upper_list_jns, 0) + end_of_section + self.list_reducer(lower_list_jns[::-1])
            plot_subsection(final_list, self.speed_limit, self.up_or_down)

    def list_reducer(self, l, p=1):
        output_l = list()
        last_elements = list()
        if p == 0:
            output_l.append(l.pop(0))
        output_l.append(l.pop(0))
        last_elements.append(l.pop(-1))
        if p == 1:
            last_elements.append(l.pop(-1))
        for n in range(int(len(l)/2)):
            a = l[(2*n)]
            b = l[(2*n + 1)]
            out_long = (a[0] + b[0])/2
            out_lat = (a[1] + b[1])/2
            output_l.append((out_long, out_lat))
        output_l += last_elements[::-1]
        return output_l

    def __repr__(self):
        output = str(self.intermediate_jns)
        return output

    def calculate_section_length(self):
        return self.end_milestone - self.start_milestone

    def get_intermediate_jns(self, preceding_jn, succeeding_jn, jns_in_section, kms_in_section):
        p = self.section.locate_jn_in_section(preceding_jn).milestone
        q = self.section.locate_jn_in_section(succeeding_jn).milestone
        intermediate_jns = list()
        if p < q:
            while p < q:
                delta_change = 0.00001
                advanced_jn_milestone = smallest_number_greater_than_number(kms_in_section, p + delta_change)
                advanced_jn = jns_in_section[kms_in_section.index(advanced_jn_milestone)]
                p = self.section.locate_jn_in_section(advanced_jn).milestone

                if p != q:
                    intermediate_jns.append(advanced_jn)
        return intermediate_jns

    def find_preceding_succeeding_junction(self):
        start_of_sr = largest_number_less_than_number(kms_in_section, self.start_milestone)
        a = jns_in_section[kms_in_section.index(start_of_sr)]
        end_of_sr = smallest_number_greater_than_number(kms_in_section, self.end_milestone)
        b = jns_in_section[kms_in_section.index(end_of_sr)]
        object_to_return = (a, b)
        return object_to_return

    def locate_start_and_end_of_SR_get_appropriate_start_and_end(self):
        if len(self.intermediate_jns) != 0:
            start_junction_pair_to_consider = (self.preceding_jn, self.intermediate_jns[0])
            end_junction_pair_to_consider = (self.succeeding_jn, self.intermediate_jns[-1])
        else:
            start_junction_pair_to_consider = (self.preceding_jn, self.succeeding_jn)
            end_junction_pair_to_consider = (self.succeeding_jn, self.preceding_jn)
        return (start_junction_pair_to_consider, end_junction_pair_to_consider)

    def split_a_section(self, origin_junction_coord, destn_junction_coord, dist, sectional_distance):

        split_coord_long = origin_junction_coord[0] + dist*(destn_junction_coord[0] - origin_junction_coord[0])/sectional_distance
        split_coord_lat = origin_junction_coord[1] + dist*(destn_junction_coord[1] - origin_junction_coord[1])/sectional_distance
        return (split_coord_long, split_coord_lat)

    def locate_generic_start_or_end(self, pair, sr_milestone):
        p = self.section.locate_jn_in_section(pair[0]).milestone
        q = self.section.locate_jn_in_section(pair[1]).milestone
        p_cord = (pair[0].longitude, pair[0].latitude)
        q_cord = (pair[1].longitude, pair[1].latitude)
        milestone_dist_between_junctions = q - p
        sr_distance = sr_milestone - p
        SR_coord = self.split_a_section(p_cord, q_cord, sr_distance, milestone_dist_between_junctions)
        return SR_coord

    def locate_start_and_end_of_SR(self):
        start_junction_pair_to_consider, end_junction_pair_to_consider = self.locate_start_and_end_of_SR_get_appropriate_start_and_end()
        start_sr_coord = self.locate_generic_start_or_end(start_junction_pair_to_consider, self.start_milestone)
        end_sr_coord = self.locate_generic_start_or_end(end_junction_pair_to_consider, self.end_milestone)
        return (start_sr_coord, end_sr_coord)

    def find_perpendicular(self, co1, co2, dist):
        s = (co1.latitude - co2.latitude)/(co1.longitude - co2.longitude)
        slope_perpendicular = -(1/s)
        m_factor = 0.000008997
        radius = dist*m_factor
        start_longitudes = self.calculate_coord_with_dist(co1.longitude, radius, slope_perpendicular, 1)
        end_longitudes = self.calculate_coord_with_dist(co2.longitude, radius, slope_perpendicular, 1)
        start_latitudes = self.calculate_coord_with_dist(co1.latitude, radius, slope_perpendicular, slope_perpendicular)
        end_latitudes = self.calculate_coord_with_dist(co2.latitude, radius, slope_perpendicular, slope_perpendicular)
        first_pair = [(start_longitudes[0], start_latitudes[0]), (end_longitudes[0], end_latitudes[0])]
        second_pair = [(start_longitudes[1], start_latitudes[1]), (end_longitudes[1], end_latitudes[1])]
        if slope_perpendicular < 0:
            first_pair, second_pair = second_pair, first_pair
        return (first_pair, second_pair)

    def calculate_coord_with_dist(self, p, l, m, var):
        sf = (var*l)*(1/(1+(m)**2))**0.5
        return (p + sf, p - sf)


def largest_number_less_than_number(list1, number):
    if not number:
        number = min(list1)
    number = float(number)
    list2 = [i for i in list1 if i <= number]
    return max(list2)


def smallest_number_greater_than_number(list1, number):
    number = float(number)
    list2 = [i for i in list1 if i >= number]
    return min(list2)


def map_attribs_to_object(attribute, list_of_attribval, list_of_objects):
    output_list = list()
    for attribval in list_of_attribval:
        v = obj_by_attribval(attribute, attribval, list_of_objects)
        output_list.append(v)

    return output_list


def obj_by_attribval(attribute, value, list_of_objects):
    for obj in list_of_objects:
        if getattr(obj, attribute) == value:
            return obj


def milestone_to_decimal(x):
    x = str(x)
    return float(x.replace('/', '.'))


def create_section_objects(list_of_sections, all_sections):
    for s in list_of_sections:
        this_section = section(s)
        all_sections.add(this_section)


def create_junction_objects(sections_and_jns, all_junctions):
    list_of_jns = sections_and_jns.return_unique_list(0)
    for jn in list_of_jns:
        temp_data = sections_and_jns.return_unique_elements(0, jn, 0, 2, 3)
        this_junction = junction(*temp_data)
        all_junctions.add(this_junction)


def add_junctions_to_sections(list_of_sections, sections_and_jns, all_sections):
    for s in list_of_sections:
        this_section = obj_by_attribval('section_name', s, all_sections)
        temp_jns_kms_in_section = sections_and_jns.sort_sheet_by_a_return_b(1, s, 0, 1)
        temp_jns_in_section = [i[0] for i in temp_jns_kms_in_section]
        temp_kms_in_section = [i[1] for i in temp_jns_kms_in_section]
        list_of_jn_obj_in_section = map_attribs_to_object('jn_name', temp_jns_in_section, all_junctions)
        for j in range(len(list_of_jn_obj_in_section)):
            this_section.add_jn(list_of_jn_obj_in_section[j], temp_kms_in_section[j])


def plot_junctions(all_junctions):
    junctions_long = []
    junctions_lat = []
    for j in all_junctions:
        gmap.marker(j.latitude, j.longitude, title=j.jn_name, color='red', c=None)
        junctions_long.append(j.longitude)
        junctions_lat.append(j.latitude)
    gmap.scatter(junctions_lat, junctions_long, '#3B0B39', size=1600, marker=False)


def plot_subsection(scn, sr, up_or_down):
    wid = sr*4/100
    if up_or_down == 'down':
        trunc = ceil(len(scn)/2) * -1
        scn = scn[:trunc]
        del scn[:1]
    elif up_or_down == 'up':
        trunc = ceil(len(scn)/2)
        del scn[:trunc]
        del scn[-1]

    if (sr >= 70):
        shade_of_green = (sr-70)*4
        green = 120 + shade_of_green
        if green > 255:
            green = 255
        triplet = (0, int(green), 0)

    elif (sr >= 45):
        shade_of_blue = (sr - 45)*5
        blue = 120 + shade_of_blue
        triplet = (0, 0, int(blue))
    else:
        if sr < 20:
            tr = 25
        else:
            tr = sr
        #print('red')
        shade_of_red = (tr - 20)*5.5
        red = 255 - shade_of_red
        triplet = (int(red), 0, 0)

    #print(triplet)
    color = rgb2hex(*triplet)
    #print(color)


    target_lons, target_lats = zip(*scn)
    gmap.plot(target_lats, target_lons, color, edge_width=10, marker=False)

def rgb2hex(r,g,b):
    return "#{:02x}{:02x}{:02x}".format(r,g,b)

list_of_sections = sections_and_jns.return_sheets()
create_section_objects(list_of_sections, all_sections)
create_junction_objects(sections_and_jns, all_junctions)

add_junctions_to_sections(list_of_sections, sections_and_jns, all_sections)

for sectn in all_sections:

    list_of_junctions_with_kms = sectn.list_of_junctions
    jns_in_section = [i.junction for i in list_of_junctions_with_kms]
    kms_in_section = [i.milestone for i in list_of_junctions_with_kms]

    p = sections_and_srs.lkup_table.get(sectn.section_name)

    if p:
        for i in p.collection:
            number_of_lines = i[-1]
            start_milestone = milestone_to_decimal(i[2])
            end_milestone = milestone_to_decimal(i[3])
            up_or_down = i[-2]

            speed_limit = i[1]
            date = i[4]
            sub_section_object = sub_section(sectn, start_milestone, end_milestone, speed_limit, jns_in_section, kms_in_section, number_of_lines, date, up_or_down)

plot_junctions(all_junctions)
gmap.draw("my_map2.html")

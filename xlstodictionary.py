import pandas as pd


def capture_from_xls(xls):

    xls = pd.ExcelFile(xls)
    sheet_dict = {}
    for i in xls.sheet_names:
        temp_list = []

        temp = pd.read_excel(xls, i)
        temp = temp.as_matrix()
        sheet_dict[i] = temp
    return sheet_dict

class db_lookup_array_of_tuples(object):
    def __init__(self, collection):
        self.collection = collection
        self.collection_dict = self.reoptimise()

    def reoptimise(self):
        collection = self.collection
        self.collection_dict = dict()
        for tup in collection:
            for i, v in enumerate(tup):
                self.collection_dict[(i, v)] = tup
                #This will overwrite entries where non-unique values are present. However, it is expected that user won't query with such an entry.
        return(self.collection_dict)

    def lookup_by_element(self, n, e):
        return self.collection_dict.get((n, e), None)


    def __repr__(self):
        return str(self.collection)

class dict_of_xls(object):
    
    def __init__(self, excel_input):
        lkup_table = dict()
        for k, v in excel_input.items():
            lkup_table_row = []
            for i in range(len(v)):
                lkup_table_row.append((tuple(v[i])))
            lkup_table[k] = db_lookup_array_of_tuples(lkup_table_row)
        self.lkup_table = lkup_table
    
    def __repr__(self):
        return(str(self.lkup_table))

    def operate_particular_tuple(self, function, *val):
        for k, v in self.lkup_table.items():
            for i in range(len(v.collection)):
                particular_tuple = v.collection[i]
                temp_list = list(particular_tuple)
                for value in val:
                    temp_list[value] = function(temp_list[value])
                v.collection[i] = tuple(temp_list) 
            v.reoptimise()
        return(self)

    def lookup_by_element(self, pos, val):
        output_dict = dict()
        for key, value in self.lkup_table.items():
            looked_up_row = value.lookup_by_element(pos, val)
            if looked_up_row:
                output_dict[key] = looked_up_row 
        return(output_dict)

    def return_unique_elements(self, pos, val, *out_p):
        for key, value in self.lkup_table.items():
            looked_up_row = value.lookup_by_element(pos, val)
            output = list()
            for p in out_p:
                if looked_up_row:
                    output.append(looked_up_row[p])
            output = tuple(output)
            if looked_up_row:
                break
        return(output)

    def return_unique_list(self, pos, sheet_name = False):
        output_set = set()
        for k, v in self.lkup_table.items():
        
            if sheet_name and k != sheet_name:
                pass

            else:    
                for i in range(len(v.collection)):
                    particular_tuple = v.collection[i]
                    output_set.add(particular_tuple[pos])
        return(sorted(list(output_set)))

    def return_sheets(self):
        output_list = list()
        for k, v in self.lkup_table.items():
            output_list.append(k)

        return output_list

    def return_unique_elements_in_sheet(self, sheet, pos, val, *out_p):
        value = self.lkup_table[sheet]
        
        looked_up_row = value.lookup_by_element(pos, val)
        output = list()

        for p in out_p:

            if looked_up_row:
                output.append(looked_up_row[p])
        output = tuple(output)
        
        return output

    def sort_sheet_by_a_return_b(self, pos, sheet, *vals):
        list_enum_pos = self.return_unique_list(pos, sheet)
        output_list = list()
        for v in list_enum_pos:
            appendage = self.return_unique_elements_in_sheet(sheet, pos, v, *vals)
            if len(appendage) == 1:
                output_list.append(appendage[0])
            else:
                output_list.append(appendage)
        return output_list



def read(xls):
    """
    Reads the excelsheet and returns a dictionary representation.
    """
    temp_object = dict_of_xls(capture_from_xls(xls))
    return temp_object

def manipulate(object, function, *values):
    """
    Manipulate particular columns. For example you might want to 
    """
    object.operate_particular_tuple(function, *values)
    return(object)

def lookup(object, pos, val):
    """
    In every sheet, we will get all those rows, where value at column 'pos' has value 'val'
    """
    output = object.lookup_by_element(pos, val)
    return output

def return_unique_elements(object, pos, val, *out_p):
    """
    Given a 'sheet, it will capture a random row across all sheets with value 'val' at position 'pos'. Once it does that, it gives values of output fields. Useful when there are redundant and repetitive entries in Excel across sheets. Example station coordinates.
    """
    output = object.return_unique_elements(pos, val, *out_p)
    return output

def return_unique_list(object, pos, sheet_name = False):
    """
    Given a 'sheet', returns all unique elements in a column indicated by pos. If no sheet_name is provided, all sheets are processed. Example: List of all stations in a section.
    """
    output = object.return_unique_list(pos, sheet_name)
    return output

def return_sheets(object):
    """
    Returns all sheets of an excel sheet
    """
    
    output = object.return_sheets()
    return output

def sort_sheet_by_a_return_b(object, pos, sheet, *vals):
    """
    Given a 'sheet' - this function will sort by column 'pos' and return columns 'vals'
    """
    output = object.sort_sheet_by_a_return_b(pos, sheet, *vals)
    return output

import csv
import pandas as pd
from itertools import chain

import networkx as nx
import matplotlib.pyplot as plt


def largest_number_less_than_number(list1, number):
	number = float(number)
	list2 = [i for i in list1 if i <= number]
	return max(list2)

def include_intermediate_sr_stns(list1, number1, number2):

	list1 = [i for i in list1 if i > number1]
	
	list1 = [i for i in list1 if i < number2]
	
	list1.insert(0, number1)
	list1.append(number2)

	return list1


def smallest_number_greater_than_number(list1, number):
	number = float(number)
	list2 = [i for i in list1 if i >= number]
	return min(list2)

def dictionary_append_if_exists(k,d,*args):

	if k in d.keys():
		d[k].append(args)
	else:
		d[k] = [args]
	return d


def capture_sections_from_xls(xls):
	xls = pd.ExcelFile(xls)
	sheet_dict = {}
	for i in xls.sheet_names:
		temp_list = []

		temp = pd.read_excel(xls, i)
		temp = temp.as_matrix()
		sheet_dict[i] = temp	
	return sheet_dict	



def narrow_down_sr(section, km_from, km_to, sections_junctions_kms):
	
	kilometers_jun_dict = {}

	for i in range(len(sections_junctions_kms[section])):
		k = sections_junctions_kms[section][i][1]
		v = sections_junctions_kms[section][i][0]
		kilometers_jun_dict[k] = v

	key_list = list(kilometers_jun_dict.keys())
	preceding_stn = largest_number_less_than_number(key_list, km_from)
	succeding_stn = smallest_number_greater_than_number(key_list, km_to)
	intermediate_stns_kms = include_intermediate_sr_stns(key_list, preceding_stn, succeding_stn)
	intermediate_stns = []




	for i in range(len(intermediate_stns_kms)):
		intermediate_stns.append(kilometers_jun_dict[intermediate_stns_kms[i]])
	return(intermediate_stns)



	
def split_a_section(origin_junction_coord, destn_junction_coord, dist, sectional_distance):

	split_coord = origin_junction_coord + dist*(destn_junction_coord - origin_junction_coord)/sectional_distance
	return split_coord

def airdistance(lalo1, lalo2):
	return ((lalo1[0] - lalo2[0])**2 + (lalo1[1] - lalo2[1])**2)**0.5



def locate_SR(orig_jn_kms, destn_jn_kms, origin_coords, destn_coords, sr_kms):
	sectional_distance = destn_jn_kms - orig_jn_kms
	sectional_air_distance = airdistance(origin_coords, destn_coords)

	start_dist = abs(orig_jn_kms - sr_kms)
	start_air_distance = sectional_air_distance*(start_dist/sectional_distance) 
	
	#end_dist = abs(origin_junction.kms - SR_end)
	#end_air_distance = sectional_air_distance*(end_dist/sectional_distance) 



	start_latitude = split_a_section(origin_coords[0], destn_coords[0], start_air_distance, sectional_air_distance)
	
	start_longitude = split_a_section(origin_coords[1], destn_coords[1], start_air_distance, sectional_air_distance)

	#end_latitude = split_a_section(origin_junction.latitude, destn_junction.latitude, end_air_distance, sectional_air_distance)
	#end_longitude = split_a_section(origin_junction.longitude, destn_junction.longitude, end_air_distance, sectional_air_distance)

	SR_location = ((start_latitude, start_longitude))
	return SR_location


def vr(base, num):
	r={} 
	k= str(base) + str(num)
	return(k)

def list_coords_to_dict(narrow_coords_to_consider):

	narrow_coords_to_consider_dict = {}

	for i in range(len(narrow_coords_to_consider)):
		key = vr('LKP-PNM_7.5_16.5_', i)
	
		value = narrow_coords_to_consider[i]
		
		narrow_coords_to_consider_dict[key] = value
	return 	narrow_coords_to_consider_dict


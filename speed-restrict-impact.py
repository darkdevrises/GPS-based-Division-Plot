# -*- coding: utf-8 -*-
"""
Created on Tue May 29 11:26:54 2018

@author: KPR
"""
import numpy as np
#Reference Databases
length_of_train = 1000
acc_speed_db = np.array([0, 4, 11, 20, 28, 35, 42, 46, 52, 61, 67, 71, 75, 80, 86, 90, 94, 97, 100])
acc_dist_db = np.array([11.11, 41.66666667, 86.11111111, 133.3333333, 175, 213.8888889, 244.4444444, 272.2222222, 313.8888889, 355.5555556, 383.3333333, 405.5555556, 430.5555556, 461.1111111, 488.8888889, 511.1111111, 530.5555556, 547.2222222])
acc_time_int = 20

dec_speed_db = np.array([100, 86, 73, 62, 53, 45, 38, 31, 25, 19, 14, 10, 7, 0])
dec_dist_db = np.array([516.6666667, 441.6666667, 375, 319.4444444, 272.2222222, 230.5555556, 191.6666667, 155.5555556, 122.2222222, 91.66666667, 66.66666667, 47.22222222, 19.44444444])
dec_time_int = 10


def lookup_starting_ending_speeds_to_acc(u, v, db):
    x = max(np.argwhere(db <= u))
    y = max(np.argwhere(db <= v))
    return (int(x - 1), int(y))

def lookup_starting_ending_speeds_to_dec(v, u, db):
    y = max(np.argwhere(db >= u))
    x = max(np.argwhere(db >= v))
    return (int(x - 1), int(y))

def get_distances_times(u_index, v_index, db, time_int):
    distance_traversed = 0
    time_taken = (v_index - u_index + 1)*time_int
    for i in range(u_index, v_index + 1):
        distance_traversed+=db[i]
    return(distance_traversed, time_taken)


def total_time_lost_while_acc_dec(u, v, sr_length):
    acc_u_index, acc_v_index = lookup_starting_ending_speeds_to_acc(u, v, acc_speed_db)
    dec_u_index, dec_v_index = lookup_starting_ending_speeds_to_dec(v, u, dec_speed_db)
    acc_dist, acc_time = get_distances_times(acc_v_index, acc_u_index, acc_dist_db, acc_time_int)
    dec_dist, dec_time = get_distances_times(dec_v_index, dec_u_index, dec_dist_db, dec_time_int)
    sr_dist = length_of_train + sr_length
    sr_time = (sr_dist*18)/(v*5)
    total_affected_dist = acc_dist + sr_dist + dec_dist
    total_time_taken_to_trav = acc_time + sr_time + dec_time
    normal_time = (total_affected_dist*18)/(u*5)
    lost_time = total_time_taken_to_trav - normal_time
    return lost_time

print(total_time_lost_while_acc_dec(75, 30, 6000))



